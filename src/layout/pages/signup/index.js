import React, { useState } from "react";

import { Form, Container, Row, Col } from "react-bootstrap";

export const Signup = () => {
  const [firstName, setFirstname] = useState("");
  const [secondName, setSecondname] = useState("");
  const [age, setAge] = useState("");
  const signupAction = () => {
    const formData = {
      firstName: firstName,
      secondName: secondName,
      age: age,
    };
    fetch("http://localhost:5000/signUp", {
      method: "POST",
      headers: {
        "access-control-allow-origin": "*",
        "Content-type": "application/json",
      },
      body: JSON.stringify(formData),
    }).then((res) =>
      res.json().then((data) => {
        alert(JSON.stringify(data));
      })
    );
  };
  return (
    <Container className="loginContainer">
      <Row>
        <Col xs={{ span: 4, offset: 4 }}>
          <Form.Control
            type="text"
            onChange={(e) => setFirstname(e.target.value)}
            value={firstName}
            placeholder="First Name"
            required={true}
          />
          <br />
          <Form.Control
            type="text"
            onChange={(e) => setSecondname(e.target.value)}
            value={secondName}
            placeholder="Second Name"
            required={true}
          />
          <br />
          <Form.Control
            type="text"
            onChange={(e) => setAge(e.target.value)}
            value={age}
            placeholder="Age"
            required={true}
          />
          <br />
          <Form.Control
            type="button"
            onClick={() => signupAction()}
            value="Login"
          />
        </Col>
      </Row>
    </Container>
  );
};
